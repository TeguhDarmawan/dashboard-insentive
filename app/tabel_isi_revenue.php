<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tabel_isi_revenue extends Model
{
    //
    protected $hidden = ['created_at', 'updated_at'];

    protected $table = "tabel_isi_revenue";
    protected $primaryKey = "revenue_id";
    public $timestamps = false;
    public $incrementing = false;

    protected $fillable = [
        'department_id', 
        'employee_id',
        'bulan', 
        'tahun',
        'revenue', 
        'hitung_revenue_insentive',
        'date_revenue'
    ];

}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class tabel_definisi_royalty_customer extends Model
{
    //
    protected $hidden = ['created_at', 'updated_at'];

    protected $table = "tabel_definisi_royalty_customer";
    protected $primaryKey = "id_royalty_customer";
    public $timestamps = false;
    public $incrementing = false;

    protected $fillable = [
        'percentage', 
        'tanggal_terbit', 
        'tanggal_kedaluarsa',
    ]; 

    public function setTanggalTerbitAttribute($value)
    {
        $this->attributes['tanggal_terbit'] = Carbon::parse($value);
    }

    public function setTanggalKedaluarsaAttribute($value)
    {
        // $this->attributes['tanggal_kedaluarsa'] = Carbon::parse($value);
        $this->attributes['tanggal_kedaluarsa'] = $value ? Carbon::createFromFormat('m/d/Y', $value)->toDateString() : null;
    }
}

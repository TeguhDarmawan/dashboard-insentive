<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tabel_isi_revenue_royalty extends Model
{
    //
    protected $hidden = ['created_at', 'updated_at'];

    protected $table = "tabel_isi_revenue_royalty";
    protected $primaryKey = "revenue_royalty_id";
    public $timestamps = false;
    public $incrementing = false;

    protected $fillable = [
        'department_id', 
        'employee_id',
        'revenue', 
        'hitung_revenue_royalty',
        'date_revenue'
    ];
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tabel_min_claim extends Model
{
    //
    protected $hidden = ['created_at', 'updated_at'];


    protected $table = "tabel_min_claim";
    protected $primaryKey = "sales_id";
    public $timestamps = false;
    public $incrementing = false;

    protected $fillable = [
        'employee_id', 
        'department_id', 
        'min_claim',
    ]; 
}

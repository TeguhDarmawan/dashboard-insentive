<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tabel_revenue_insentive extends Model
{
    //
    protected $hidden = ['created_at', 'updated_at'];


    protected $table = "tabel_revenue_insentive";
    protected $primaryKey = "employe_id";
    public $timestamps = false;
    public $incrementing = false;

    protected $fillable = [
        'januari_2021', 
        'februari_2021',
        'maret_2021', 
        'april_2021',
        'mei_2021', 
        'juni_2021',
        'juli_2021', 
        'agustus_2021',
        'september_2021', 
        'oktober_2021',
        'november_2021', 
        'desember_2021',
        'januari_2022', 
        'februari_2022',
        'maret_2022', 
        'april_2022',
        'mei_2022', 
        'juni_2022',
        'juli_2022', 
        'agustus_2022',
        'september_2022', 
        'oktober_2022',
        'november_2022', 
        'desember_2022'
    ]; 
}

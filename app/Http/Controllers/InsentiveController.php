<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use App\tabel_definisi_new_customer;
use App\tabel_min_claim;
use App\tabel_revenue_insentive;
use App\tabel_isi_revenue;
use App\tabel_definisi_royalty_customer;
use App\tabel_isi_revenue_royalty;

class InsentiveController extends Controller
{
    //
    public function index()
    {
        $count = tabel_definisi_new_customer::count();
        $count1 = tabel_min_claim::count();
        return view('beranda-insentive', compact('count', 'count1'));
    }

    public function rules()
    {
        $new_customer = tabel_definisi_new_customer::orderBy('percentage', 'asc')
        ->orderBy('min_revenue', 'asc')->orderBy('max_revenue','asc')->get();
        $tanggal = tabel_definisi_new_customer::first();
        return view('new-customer.rules', compact('new_customer', 'tanggal'));
    }

    public function tambah_rules()
    {
        return view('new-customer.tambah-rules');
    }

    public function proses_tambah_rules(Request $request)
    {
        $this->validate(
            $request,
            [
                'percentage' => 'required',
                'min_revenue' => 'required',
                // 'max_revenue' => 'required',
                'tanggal_terbit' => 'required',
            ],
            [
                'percentage.required' => 'Percentage harus diisi.',
                'min_revenue.required' => 'Min Revenue harus diisi.',
                // 'max_revenue.required' => 'Max Revenue harus diisi.',
                'tanggal_terbit.required' => 'Tanggal terbit harus diisi.',
            ]
        );

        if ($cek = tabel_definisi_new_customer::where('min_revenue', '<=', $request->min_revenue)->Where('max_revenue', '>=', $request->min_revenue)->first()) {
            return redirect()->back()->with('toast_error', 'Error! <br> Data range sudah dalam percentage : '.number_format($cek->percentage).'%'.'<br>'.'Silahkan dicek kembali.');
        }

        if ($cek = tabel_definisi_new_customer::where('min_revenue', '<=', $request->max_revenue)->Where('max_revenue', '>=', $request->max_revenue)->first()) {
            return redirect()->back()->with('toast_error', 'Error! <br> Data range sudah ada max percentage : '.number_format($cek->percentage).'%'.'<br>'.'Silahkan dicek kembali.');
        }
        
        tabel_definisi_new_customer::create([
                'percentage' => $request->percentage,
                'min_revenue' => $request->min_revenue,
                'max_revenue' => $request->max_revenue,
                'tanggal_terbit' => $request->tanggal_terbit,
                'tanggal_kedaluarsa' => $request->tanggal_kedaluarsa,
        ]);
        //  dd($request->all());
        return redirect('rules')->with('success', 'Rules Berhasil di Tambah');;
    }

    public function editdatarules($id){
        $rules = tabel_definisi_new_customer::findorfail($id);
        return view('new-customer.edit-rules',compact('rules'));
    }
 
    public function proses_update_rules(Request $request, $id)
    {
        // dd($request->all());
         $this->validate(
         $request,
         [
             'percentage' => 'required',
             'min_revenue' => 'required',
             'max_revenue' => 'required',
             'tanggal_terbit' => 'required',
         ],
         [
            'percentage.required' => 'Percentage harus diisi.',
            'min_revenue.required' => 'Min Revenue harus diisi.',
            'max_revenue.required' => 'Max Revenue harus diisi.',
            'tanggal_terbit.required' => 'Tanggal terbit harus diisi.',
         ]
     );
    
    //  if ($cek = tabel_definisi_new_customer::where('min_revenue', '<=', $request->min_revenue)->Where('max_revenue', '>=', $request->min_revenue)->first()) {
    //     return redirect()->back()->with('toast_error', 'Error! <br> Data range sudah dalam percentage : '.number_format($cek->percentage).'%'.'<br>'.'Silahkan dicek kembali.');
    // }

    // if ($cek = tabel_definisi_new_customer::where('min_revenue', '<=', $request->max_revenue)->Where('max_revenue', '>=', $request->max_revenue)->first()) {
    //     return redirect()->back()->with('toast_error', 'Error! <br> Data range sudah ada max percentage : '.number_format($cek->percentage).'%'.'<br>'.'Silahkan dicek kembali.');
    // }

     tabel_definisi_new_customer::where('id_new_customer', $id)->update([
             'percentage' => $request->percentage,
             'min_revenue' => $request->min_revenue,
             'max_revenue' => $request->max_revenue,
             'tanggal_terbit' => Carbon::createFromFormat('m/d/Y', $request->tanggal_terbit),
             'tanggal_kedaluarsa' => $request->tanggal_kedaluarsa ? Carbon::createFromFormat('m/d/Y', $request->tanggal_kedaluarsa) : null
         ]);
         return redirect('rules')->with('success', 'Data Rules Berhasil di Update');;
     }

    public function data_min_claim()
    {
        $data_karyawan = DB::table('employee')->where('int_emp_division', '1')->get();
        $data_min_claim = tabel_min_claim::leftJoin('department', 'department.department_id', 
        'tabel_min_claim.department_id')->leftJoin('employee', 'employee.int_emp_id',
        'tabel_min_claim.employee_id')
        ->select(
            'tabel_min_claim.sales_id',
            'tabel_min_claim.employee_id',
            'tabel_min_claim.department_id',
            'tabel_min_claim.min_claim',
            'department.department_name as nama_departemen',
            'employee.int_emp_name as nama_karyawan'

        )
        ->get();
        $nama_karyawan_sales = DB::table('tabel_sales_team')->get();
        return view('data-min-claim', compact('data_min_claim', 'data_karyawan', 'nama_karyawan_sales'));
    }

    public function findDepartmentName(Request $request)
    {
        $p = DB::table('employee')->select('employee.int_emp_department', 'department.department_name as department_name')
        ->leftJoin('department', 'department.department_id', 'employee.int_emp_department')
        ->where('int_emp_name',$request->id)->first();
    	return response()->json($p);
    }
    
    public function findSalesEmployeeName(Request $request){

        $data = DB::table('employee')->select('int_emp_name', 'int_emp_id')->where('int_emp_department',$request->id)->take(100)->get();
        return response()->json($data);//then sent this data to ajax success
	}

    public function tambah_min_claim()
    {
        $data_karyawan = DB::table('employee')->where('int_emp_division', '1')->get();
        $data_department = DB::table('department')->where('department_id', '4')
        ->orwhere('department_id', '5')->orwhere('department_id', '6')->orwhere('department_id', '7')
        ->get();
        $data_min_claim = tabel_min_claim::leftJoin('employee', 'employee.int_emp_id', 
        'tabel_min_claim.employee_id')
        ->select(
            'tabel_min_claim.sales_id',
            'tabel_min_claim.employee_id',
            'tabel_min_claim.department_id',
            'tabel_min_claim.min_claim',
            'employee.int_emp_name as nama_karyawan'
        )
        ->get();
        return view('tambah-min-claim', compact('data_min_claim', 'data_karyawan', 'data_department'));
    }

    public function proses_tambah_data_min_claim(Request $request)
    {
        $this->validate(
            $request,
            [
                'employee_id' => 'required|unique:tabel_min_claim',
                'department_id' => 'required',
                'min_claim' => 'required',
            ],
            [
                'employee_id.required' => 'Employee harus diisi.',
                'employee_id.unique' => 'Employee Name sudah ada di list data min claim, silahkan dicek kembali.',
                'department_id.required' => 'Departemen harus diisi.',
                'min_claim.required' => 'Min claim harus diisi.'
            ]
        );
        
        if ($cek = tabel_min_claim::where('employee_id', '=', $request->employee_id)->first()) {
            return redirect()->back()->with('success', 'Error! Data min claim sudah ada');
        }

        tabel_min_claim::create([
                'employee_id' => $request->employee_id,
                'department_id' => $request->department_id,
                'min_claim' => $request->min_claim,
        ]);
        return redirect('data-min-claim')->with('success', 'Data Min Claim Berhasil di Tambah');;
    }
    

    public function editminclaim($id){
        $data_min_claim = tabel_min_claim::findorfail($id);
        $data_min_claim_join = tabel_min_claim::leftJoin('department', 'department.department_id', 
        'tabel_min_claim.department_id')->leftJoin('employee', 'employee.int_emp_id',
        'tabel_min_claim.employee_id')
        ->select(
            'tabel_min_claim.sales_id',
            'tabel_min_claim.employee_id',
            'tabel_min_claim.department_id',
            'tabel_min_claim.min_claim',
            'department.department_name as nama_departemen',
            'employee.int_emp_name as nama_karyawan'
        )->get();
        return view('edit-minclaim',compact('data_min_claim', 'data_min_claim_join'));
    }
 
    public function proses_update_minclaim(Request $request, $id)
    {
        // dd($request->all());
         $this->validate(
         $request,
         [
            //  'department_id' => 'required',
            //  'employee_id' => 'required',
             'min_claim' => 'required'
         ],
         [
            // 'department_id.required' => 'Department harus diisi.',
            // 'employee_id.required' => 'Employee Name harus diisi.',
            'min_claim.required' => 'Min claim harus diisi.',
         ]
     );

     tabel_min_claim::where('sales_id', $id)->update([
            //  'department_id' => $request->department_id,
            //  'employee_id' => $request->employee_id,
             'min_claim' => $request->min_claim
         ]);
         return redirect('data-min-claim')->with('success', 'Data Min claim Berhasil di Update');;
     }


    public function tabel_isi_data_revenue()
    {
        $data_karyawan = DB::table('employee')->where('int_emp_division', '1')->get();
        // $tabel_revenue = tabel_revenue_insentive::all();
        // $tabel_revenue = tabel_isi_revenue::all();
        $data_department = DB::table('department')->where('department_id', '4')
        ->orwhere('department_id', '5')->orwhere('department_id', '6')->orwhere('department_id', '7')
        ->get();
        $tabel_revenue = tabel_isi_revenue::leftJoin('department', 'department.department_id', 
        'tabel_isi_revenue.department_id')->leftJoin('employee', 'employee.int_emp_id',
        'tabel_isi_revenue.employee_id')
        ->select(
            'tabel_isi_revenue.revenue_id',
            'tabel_isi_revenue.employee_id',
            'tabel_isi_revenue.department_id',
            'tabel_isi_revenue.date_revenue',
            'tabel_isi_revenue.revenue',
            'department.department_name as nama_departemen',
            'employee.int_emp_name as nama_karyawan'
        )->get();
        return view('tabel-revenue', compact('tabel_revenue', 'data_karyawan', 'data_department'));
    }

    public function action(Request $request)
    {
    	if($request->ajax())
    	{
    		if($request->action == 'edit')
    		{
    			$data = array(
    				'employee_name'	    =>	$request->employee_name,
    				'januari_2021'		=>	$request->januari_2021,
                    'februari_2021'		=>	$request->februari_2021,
                    'maret_2021'		=>	$request->maret_2021,
                    'april_2021'		=>	$request->april_2021,
                    'mei_2021'		    =>	$request->mei_2021,
                    'juni_2021'	    	=>	$request->juni_2021,
                    'juli_2021'	    	=>	$request->juli_2021,
                    'agustus_2021'		=>	$request->agustus_2021,
                    'september_2021'	=>	$request->september_2021,
                    'november_2021'		=>	$request->november_2021,
                    'desember_2021'		=>	$request->desember_2021,
                    'januari_2022'		=>	$request->januari_2022,
                    'februari_2022'		=>	$request->februari_2022,
                    'maret_2022'		=>	$request->maret_2022,
                    'april_2022'		=>	$request->april_2022,
                    'mei_2022'		    =>	$request->mei_2022,
                    'juni_2022'	    	=>	$request->juni_2022,
                    'juli_2022'	    	=>	$request->juli_2022,
                    'agustus_2022'		=>	$request->agustus_2022,
                    'september_2022'	=>	$request->september_2022,
                    'november_2022'		=>	$request->november_2022,
                    'desember_2022'		=>	$request->desember_2022,
                    
    			);
    			DB::table('tabel_revenue_insentive')
    				->where('employee_id', $request->employee_id)
    				->update($data);
    		}
    		if($request->action == 'delete')
    		{
    			DB::table('tabel_revenue_insentive')
    				->where('employee_id', $request->employee_id)
    				->delete();
    		}
    		return response()->json($request);
    	}
    }

    public function tambah_isi_data_revenue()
    {
        $data_karyawan = DB::table('employee')->where('int_emp_division', '1')->get();
        $data_department = DB::table('department')->where('department_id', '4')
        ->orwhere('department_id', '5')->orwhere('department_id', '6')->orwhere('department_id', '7')
        ->get();
        $data_revenue = tabel_isi_revenue::leftJoin('employee', 'employee.int_emp_id', 
        'tabel_isi_revenue.employee_id')
        ->select(
            'tabel_isi_revenue.revenue_id',
            'tabel_isi_revenue.employee_id',
            'tabel_isi_revenue.department_id',
            'tabel_isi_revenue.revenue',
            'tabel_isi_revenue.date_revenue',
            'employee.int_emp_name as nama_karyawan'
        )
        ->get();
        return view('tambah-isi-data-revenue', compact('data_revenue', 'data_karyawan', 'data_department'));
    }

    public function proses_tambah_data_revenue(Request $request)
    {
        $this->validate(
            $request,
            [
                'department_id' => 'required',
                'employee_id' => 'required',
                'revenue' => 'required',
                'date_revenue' => 'required'
            ],
            [
                'department_id.required' => 'Departemen harus diisi.',
                'employee_id.required' => 'Employee harus diisi.',
                'revenue' => 'revenue harus diisi',
                'date_revenue.required' => 'Date revenue harus diisi'
            ]
        );
        
        $persen = 0;
        $cekRule1 = tabel_definisi_new_customer::where('min_revenue', '<=', $request->revenue)->where('max_revenue', '>=', $request->revenue)->where('tanggal_terbit', '<=', $request->date_revenue)->Where('tanggal_kedaluarsa', '>=', $request->date_revenue)->first();
        $cekRule2 = tabel_definisi_new_customer::where('min_revenue', '<=', $request->revenue)->where('max_revenue', '>=', $request->revenue)->where('tanggal_terbit', '<=', $request->date_revenue)->WhereNull('tanggal_kedaluarsa')->first();
        //$claim = tabel_min_claim::where('employee_id', '=', $request->employee_id)->first();
        if ($cekRule1 != null)
        {
            $persen = $cekRule1->percentage;
        }
        if ( $cekRule2 != null)
        {
            $persen = $cekRule2->percentage;
        }
        // if ($request->revenue*$persen->percentage/100 < $claim->min_claim)
        // {
        //     $hitung_claim = 0;
        // }
        // if ($request->revenue*$persen->percentage/100 >= $claim->min_claim)
        // {
        //     $hitung_claim = $request->revenue*$persen->percentage/100;
        // }
        tabel_isi_revenue::create([
                'department_id' => $request->department_id,
                'employee_id' => $request->employee_id,
                'bulan' => $request->bulan,
                'tahun' => $request->tahun,
                'revenue' => $request->revenue,
                'hitung_revenue_insentive' => $request->revenue*$persen/100,
                'date_revenue' => $request->date_revenue
        ]);
        // dd($request->all());
        return redirect('tabel-isi-data-revenue')->with('success', 'Data isi Revenue Berhasil di Tambah');
    }

    public function editdatarevenue($id){
        $revenue = tabel_isi_revenue::findorfail($id);
        $data_revenue = tabel_isi_revenue::leftJoin('department', 'department.department_id', 
        'tabel_isi_revenue.department_id')->leftJoin('employee', 'employee.int_emp_id',
        'tabel_isi_revenue.employee_id')
        ->select(
            'tabel_isi_revenue.revenue_id',
            'tabel_isi_revenue.employee_id',
            'tabel_isi_revenue.department_id',
            'tabel_isi_revenue.date_revenue',
            'tabel_isi_revenue.revenue',
            'department.department_name as nama_departemen',
            'employee.int_emp_name as nama_karyawan'
        )->get();
        return view('edit-revenue',compact('revenue', 'data_revenue'));
    }
 
    public function proses_update_revenue(Request $request, $id)
    {
        // dd($request->all());
         $this->validate(
         $request,
         [
             'revenue' => 'required',
             'date_revenue' => 'required'
         ],
         [
            'revenue.required' => 'Revenue harus diisi.',
            'date_revenue.required' => 'Date Revenue harus diisi.'
         ]
     );

     $persen = 0;
        $cekRule1 = tabel_definisi_new_customer::where('min_revenue', '<=', $request->revenue)->where('max_revenue', '>=', $request->revenue)->where('tanggal_terbit', '<=', $request->date_revenue)->Where('tanggal_kedaluarsa', '>=', $request->date_revenue)->first();
        $cekRule2 = tabel_definisi_new_customer::where('min_revenue', '<=', $request->revenue)->where('max_revenue', '>=', $request->revenue)->where('tanggal_terbit', '<=', $request->date_revenue)->WhereNull('tanggal_kedaluarsa')->first();
        $claim = tabel_min_claim::where('employee_id', '=', $request->employee_id)->first();
        if ($cekRule1 != null)
        {
            $persen = $cekRule1->percentage;
        }
        if ( $cekRule2 != null)
        {
            $persen = $cekRule2->percentage;
        }
        if ($request->revenue*$persen->percentage/100 < $claim->min_claim)
        {
            $hitung_insentive = 0;
        }
        if ($request->revenue*$persen->percentage/100 >= $claim->min_claim)
        {
            $hitung_insentive = $request->revenue*$persen->percentage/100;
        }
    
     tabel_isi_revenue::where('revenue_id', $id)->update([
             'hitung_revenue_insentive' => $request->hitung_claim,
             'revenue' => $request->revenue,
             'date_revenue' => $request->date_revenue
         ]);
         return redirect('tabel-isi-data-revenue')->with('success', 'Data Revenue Berhasil di Update');;
     }


    public function hapus_revenue($id)
    {
	    DB::table('tabel_isi_revenue')->where('revenue_id',$id)->delete();
	    return redirect('tabel-isi-data-revenue');
    }   

    public function data_new_revenue()
    {
        $test = tabel_isi_revenue::leftJoin('employee', 'employee.int_emp_id', 'tabel_isi_revenue.employee_id')->whereYear('date_revenue','=', 2021)
        ->leftJoin('tabel_min_claim', 'tabel_min_claim.sales_id', 'tabel_isi_revenue.employee_id')
        ->select(DB::raw('employee.int_emp_id, employee.int_emp_number, employee.int_emp_name, 
        tabel_min_claim.sales_id, tabel_min_claim.employee_id, tabel_min_claim.min_claim,
        SUM(CASE WHEN MONTH(tabel_isi_revenue.date_revenue) = 1 THEN tabel_isi_revenue.hitung_revenue_insentive ELSE 0 END) as Jan,
        SUM(CASE WHEN MONTH(tabel_isi_revenue.date_revenue) = 2 THEN tabel_isi_revenue.hitung_revenue_insentive ELSE 0 END) as Feb,
        SUM(CASE WHEN MONTH(tabel_isi_revenue.date_revenue) = 3 THEN tabel_isi_revenue.hitung_revenue_insentive ELSE 0 END) as Mar,
        SUM(CASE WHEN MONTH(tabel_isi_revenue.date_revenue) = 4 THEN tabel_isi_revenue.hitung_revenue_insentive ELSE 0 END) as Apr,
        SUM(CASE WHEN MONTH(tabel_isi_revenue.date_revenue) = 5 THEN tabel_isi_revenue.hitung_revenue_insentive ELSE 0 END) as May,
        SUM(CASE WHEN MONTH(tabel_isi_revenue.date_revenue) = 6 THEN tabel_isi_revenue.hitung_revenue_insentive ELSE 0 END) as Jun,
        SUM(CASE WHEN MONTH(tabel_isi_revenue.date_revenue) = 7 THEN tabel_isi_revenue.hitung_revenue_insentive ELSE 0 END) as Jul,
        SUM(CASE WHEN MONTH(tabel_isi_revenue.date_revenue) = 8 THEN tabel_isi_revenue.hitung_revenue_insentive ELSE 0 END) as Aug,
        SUM(CASE WHEN MONTH(tabel_isi_revenue.date_revenue) = 9 THEN tabel_isi_revenue.hitung_revenue_insentive ELSE 0 END) as Sep,
        SUM(CASE WHEN MONTH(tabel_isi_revenue.date_revenue) = 10 THEN tabel_isi_revenue.hitung_revenue_insentive ELSE 0 END) as Oct,
        SUM(CASE WHEN MONTH(tabel_isi_revenue.date_revenue) = 11 THEN tabel_isi_revenue.hitung_revenue_insentive ELSE 0 END) as Nov,
        SUM(CASE WHEN MONTH(tabel_isi_revenue.date_revenue) = 12 THEN tabel_isi_revenue.hitung_revenue_insentive ELSE 0 END) as Des'))

        ->groupBy('employee.int_emp_id','employee.int_emp_number','employee.int_emp_name',
                  'tabel_min_claim.sales_id', 'tabel_min_claim.employee_id', 'tabel_min_claim.min_claim')
    ->get();
        //dd($test);
        return view('data-new-revenue', compact('test'));
    }

    
    public function data_royalty_customer()
    {
        $royalty_customer = tabel_definisi_royalty_customer::orderBy('percentage', 'asc')->get();
        return view('royalty-customer.data-royalty-customer', compact('royalty_customer'));
    }

    public function tambah_royalty_customer()
    {
        return view('royalty-customer.tambah-royalty-customer');
    }

    public function proses_tambah_royalty_customer(Request $request)
    {
        $this->validate(
            $request,
            [
                'percentage' => 'required',
                'tanggal_terbit' => 'required',
            ],
            [
                'percentage.required' => 'Percentage harus diisi.',
                'tanggal_terbit.required' => 'Tanggal terbit harus diisi.',
            ]
        );
        
        tabel_definisi_royalty_customer::create([
                'percentage' => $request->percentage,
                'tanggal_terbit' => $request->tanggal_terbit,
                'tanggal_kedaluarsa' => $request->tanggal_kedaluarsa,
        ]);
        //  dd($request->all());
        return redirect('data-royalty-customer')->with('success', 'Rules Berhasil di Tambah');;
    }

    public function editdataroyaltycustomer($id){
        $rules_royalty = tabel_definisi_royalty_customer::findorfail($id);
        return view('royalty-customer.edit-royalty-customer',compact('rules_royalty'));
    }
 
    public function proses_update_royaltycustomer(Request $request, $id)
    {
        // dd($request->all());
         $this->validate(
         $request,
         [
             'percentage' => 'required',
             'tanggal_terbit' => 'required',
         ],
         [
            'percentage.required' => 'Percentage harus diisi.',
            'tanggal_terbit.required' => 'Tanggal terbit harus diisi.',
         ]
     );

     tabel_definisi_royalty_customer::where('id_royalty_customer', $id)->update([
             'percentage' => $request->percentage,
             'tanggal_terbit' => Carbon::createFromFormat('m/d/Y', $request->tanggal_terbit),
             'tanggal_kedaluarsa' => $request->tanggal_kedaluarsa ? Carbon::createFromFormat('m/d/Y', $request->tanggal_kedaluarsa) : null
         ]);
         return redirect('data-royalty-customer')->with('success', 'Rules Royalty Berhasil di Update');;
     }

    public function isi_data_revenue_royalty()
    {
        $data_karyawan = DB::table('employee')->where('int_emp_division', '1')->get();
        $data_department = DB::table('department')->where('department_id', '4')
        ->orwhere('department_id', '5')->orwhere('department_id', '6')->orwhere('department_id', '7')
        ->get();
        $tabel_revenue_royalty = tabel_isi_revenue_royalty::leftJoin('department', 'department.department_id', 
        'tabel_isi_revenue_royalty.department_id')->leftJoin('employee', 'employee.int_emp_id',
        'tabel_isi_revenue_royalty.employee_id')
        ->select(
            'tabel_isi_revenue_royalty.revenue_royalty_id',
            'tabel_isi_revenue_royalty.employee_id',
            'tabel_isi_revenue_royalty.department_id',
            'tabel_isi_revenue_royalty.date_revenue',
            'tabel_isi_revenue_royalty.revenue',
            'department.department_name as nama_departemen',
            'employee.int_emp_name as nama_karyawan'
        )->get();
        return view('isi-data-revenue-royalty', compact('data_karyawan', 'data_department', 'tabel_revenue_royalty'));
    }

    public function tambah_isi_data_revenue_royalty()
    {
        $data_karyawan = DB::table('employee')->where('int_emp_division', '1')->get();
        $data_department = DB::table('department')->where('department_id', '4')
        ->orwhere('department_id', '5')->orwhere('department_id', '6')->orwhere('department_id', '7')
        ->get();
        $data_revenue_royalty = tabel_isi_revenue_royalty::leftJoin('employee', 'employee.int_emp_id', 
        'tabel_isi_revenue_royalty.employee_id')
        ->select(
            'tabel_isi_revenue_royalty.revenue_royalty_id',
            'tabel_isi_revenue_royalty.employee_id',
            'tabel_isi_revenue_royalty.department_id',
            'tabel_isi_revenue_royalty.revenue',
            'tabel_isi_revenue_royalty.date_revenue',
            'employee.int_emp_name as nama_karyawan'
        )
        ->get();
        return view('tambah-isi-data-revenue-royalty', compact('data_revenue_royalty', 'data_karyawan', 'data_department'));
    }

    public function proses_tambah_data_revenue_royalty(Request $request)
    {
        $this->validate(
            $request,
            [
                'department_id' => 'required',
                'employee_id' => 'required',
                'revenue' => 'required',
                'date_revenue' => 'required'
            ],
            [
                'department_id.required' => 'Departemen harus diisi.',
                'employee_id.required' => 'Employee harus diisi.',
                'revenue' => 'revenue harus diisi',
                'date_revenue.required' => 'Date revenue harus diisi'
            ]
        );
        
        $persen = 0;
        $cekRule1 = tabel_definisi_royalty_customer::where('tanggal_terbit', '<=', $request->date_revenue)->Where('tanggal_kedaluarsa', '>=', $request->date_revenue)->first();
        $cekRule2 = tabel_definisi_royalty_customer::where('tanggal_terbit', '<=', $request->date_revenue)->WhereNull('tanggal_kedaluarsa')->first();
        if ($cekRule1 != null)
        {
            $persen = $cekRule1->percentage;
        }
        if ( $cekRule2 != null)
        {
            $persen = $cekRule2->percentage;
        }
  
        tabel_isi_revenue_royalty::create([
                'department_id' => $request->department_id,
                'employee_id' => $request->employee_id,
                'revenue' => $request->revenue,
                'hitung_revenue_royalty' => $request->revenue*$persen/100,
                'date_revenue' => $request->date_revenue
        ]);
        // dd($request->all());
        return redirect('isi-data-revenue-royalty')->with('success', 'Data isi Revenue Royalty Berhasil di Tambah');
    }

    public function data_revenue_royalty()
    {
        $test = tabel_isi_revenue_royalty::leftJoin('employee', 'employee.int_emp_id', 'tabel_isi_revenue_royalty.employee_id')->whereYear('date_revenue', '=', date('Y'))
        ->select(DB::raw('employee.int_emp_id, employee.int_emp_number, employee.int_emp_name, 
        SUM(CASE WHEN MONTH(tabel_isi_revenue_royalty.date_revenue) = 1 THEN tabel_isi_revenue_royalty.hitung_revenue_royalty ELSE 0 END) as Jan,
        SUM(CASE WHEN MONTH(tabel_isi_revenue_royalty.date_revenue) = 2 THEN tabel_isi_revenue_royalty.hitung_revenue_royalty ELSE 0 END) as Feb,
        SUM(CASE WHEN MONTH(tabel_isi_revenue_royalty.date_revenue) = 3 THEN tabel_isi_revenue_royalty.hitung_revenue_royalty ELSE 0 END) as Mar,
        SUM(CASE WHEN MONTH(tabel_isi_revenue_royalty.date_revenue) = 4 THEN tabel_isi_revenue_royalty.hitung_revenue_royalty ELSE 0 END) as Apr,
        SUM(CASE WHEN MONTH(tabel_isi_revenue_royalty.date_revenue) = 5 THEN tabel_isi_revenue_royalty.hitung_revenue_royalty ELSE 0 END) as May,
        SUM(CASE WHEN MONTH(tabel_isi_revenue_royalty.date_revenue) = 6 THEN tabel_isi_revenue_royalty.hitung_revenue_royalty ELSE 0 END) as Jun,
        SUM(CASE WHEN MONTH(tabel_isi_revenue_royalty.date_revenue) = 7 THEN tabel_isi_revenue_royalty.hitung_revenue_royalty ELSE 0 END) as Jul,
        SUM(CASE WHEN MONTH(tabel_isi_revenue_royalty.date_revenue) = 8 THEN tabel_isi_revenue_royalty.hitung_revenue_royalty ELSE 0 END) as Aug,
        SUM(CASE WHEN MONTH(tabel_isi_revenue_royalty.date_revenue) = 9 THEN tabel_isi_revenue_royalty.hitung_revenue_royalty ELSE 0 END) as Sep,
        SUM(CASE WHEN MONTH(tabel_isi_revenue_royalty.date_revenue) = 10 THEN tabel_isi_revenue_royalty.hitung_revenue_royalty ELSE 0 END) as Oct,
        SUM(CASE WHEN MONTH(tabel_isi_revenue_royalty.date_revenue) = 11 THEN tabel_isi_revenue_royalty.hitung_revenue_royalty ELSE 0 END) as Nov,
        SUM(CASE WHEN MONTH(tabel_isi_revenue_royalty.date_revenue) = 12 THEN tabel_isi_revenue_royalty.hitung_revenue_royalty ELSE 0 END) as Des'))
        ->groupBy('employee.int_emp_id','employee.int_emp_number','employee.int_emp_name')
    ->get();
        //dd($test);
        return view('data-revenue-royalty', compact('test'));
    }
    

























    
     // public function hitung()
    // {
    //     return view('hitung');
    // }

        // public function data_min_claim2()
    // {
    //     $data_karyawan = DB::table('employee')->where('int_emp_division', '1')->get();
    //     $data_min_claim = tabel_min_claim::leftJoin('employee', 'employee.int_emp_id', 
    //     'tabel_min_claim.employee_id')
    //     ->select(
    //         'tabel_min_claim.employee_id',
    //         'tabel_min_claim.department_id',
    //         'tabel_min_claim.min_claim',
    //         'employee.int_emp_name as nama_karyawan'
    //     )
    //     ->get();
    //     $nama_karyawan_sales = DB::table('tabel_sales_team')->get();
    //     return view('data-min-claim2', compact('data_min_claim', 'data_karyawan', 'nama_karyawan_sales'));
    // }
    
}

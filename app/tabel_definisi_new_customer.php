<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class tabel_definisi_new_customer extends Model
{
    //
    protected $hidden = ['created_at', 'updated_at'];

    // protected $dates = [
    //     'dates'
    // ];

    protected $table = "tabel_definisi_new_customer";
    protected $primaryKey = "id_new_customer";
    public $timestamps = false;
    public $incrementing = false;

    protected $fillable = [
        'percentage', 
        'min_revenue', 
        'max_revenue', 
        'tanggal_terbit', 
        'tanggal_kedaluarsa',
    ]; 

    // public function setMinRevenueAttribute($value)
    // {
    //     $this->attributes['min_revenue'] = str_replace('.', '', $value);
    // }

    public function setTanggalTerbitAttribute($value)
    {
        $this->attributes['tanggal_terbit'] = Carbon::parse($value);
    }

    public function setTanggalKedaluarsaAttribute($value)
    {
        // $this->attributes['tanggal_kedaluarsa'] = Carbon::parse($value);
        $this->attributes['tanggal_kedaluarsa'] = $value ? Carbon::createFromFormat('m/d/Y', $value)->toDateString() : null;
    }
}

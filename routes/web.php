<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('beranda-insentive')->name('beranda-insentive');
});

Route::get('/', 'InsentiveController@index')->name('beranda-insentive');

/*-----------------------------Controller untuk menu tabel #1 new customer->rules---------------------------------- */
Route::get('/rules', 'InsentiveController@rules')->name('rules');
Route::get('/tambah-rules', 'InsentiveController@tambah_rules')->name('tambah-rules');
Route::post('/tambah/proses/rules', 'InsentiveController@proses_tambah_rules');
Route::get('/editdatarules/{id}', 'InsentiveController@editdatarules')->name('edit-data-rules');
Route::patch('update-proses-datarules/{id}', 'InsentiveController@proses_update_rules')->name('update-proses-rules');


/*-----------------------------Controller untuk menu tabel #1 min claim-------------------------------------------- */
Route::get('/data-min-claim', 'InsentiveController@data_min_claim')->name('data-min-claim');
Route::get('/tambah-min-claim', 'InsentiveController@tambah_min_claim')->name('tambah-min-claim');
Route::post('/tambah/proses/data-min-claim', 'InsentiveController@proses_tambah_data_min_claim');
Route::get('/findDepartmentName','InsentiveController@findDepartmentName');
Route::get('/findSalesEmployeeName','InsentiveController@findSalesEmployeeName');
Route::get('/editminclaim/{id}', 'InsentiveController@editminclaim')->name('edit-min-claim');
Route::patch('update-proses-minclaim/{id}', 'InsentiveController@proses_update_minclaim')->name('update-proses-minclaim');


/*-----------------------------Controller untuk menu tabel #2 isi data revenue---------------------------------------- */
Route::get('/tabel-isi-data-revenue', 'InsentiveController@tabel_isi_data_revenue')->name('tabel-isi-data-revenue');
Route::post('tabel-isi-data-revenue/action', 'InsentiveController@action')->name('tabel-isi-data-revenue.action');
Route::get('/tambah-isi-data-revenue', 'InsentiveController@tambah_isi_data_revenue')->name('tambah-isi-data-revenue');
Route::post('/tambah/proses/data-revenue', 'InsentiveController@proses_tambah_data_revenue');
Route::get('/editdatarevenue/{id}', 'InsentiveController@editdatarevenue')->name('edit-data-revenue');
Route::patch('update-proses-datarevenue/{id}', 'InsentiveController@proses_update_revenue')->name('update-proses-revenue');
Route::get('/hapusdatarevenue/{id}','InsentiveController@hapus_revenue');

/*-----------------------------Controller untuk menu tabel #3 insentive revenue-------------------------------------- */
Route::get('/data-new-revenue', 'InsentiveController@data_new_revenue')->name('data-new-revenue');


/*-----------------------------Controller untuk menu tabel #1 Royalty Customer--------------------------------------- */
Route::get('/data-royalty-customer', 'InsentiveController@data_royalty_customer')->name('data-royalty-customer');
Route::get('/tambah-royalty-customer', 'InsentiveController@tambah_royalty_customer')->name('tambah-royalty-customer');
Route::post('/tambah/proses/royaltycustomer', 'InsentiveController@proses_tambah_royalty_customer');
Route::get('/editdataroyaltycustomer/{id}', 'InsentiveController@editdataroyaltycustomer')->name('edit-data-royalty-customer');
Route::patch('update-proses-dataroyaltycustomer/{id}', 'InsentiveController@proses_update_royaltycustomer')->name('update-proses-royaltycustomer');


/*-----------------------------Controller untuk menu tabel #4 Royalty Customer--------------------------------------- */
Route::get('/isi-data-revenue-royalty', 'InsentiveController@isi_data_revenue_royalty')->name('isi-data-revenue-royalty');
Route::get('/tambah-isi-data-revenue-royalty', 'InsentiveController@tambah_isi_data_revenue_royalty')->name('tambah-isi-data-revenue-royalty');
Route::post('/tambah/proses/data-revenue-royalty', 'InsentiveController@proses_tambah_data_revenue_royalty');
Route::get('/data-revenue-royalty', 'InsentiveController@data_revenue_royalty')->name('data-revenue-royalty');



Route::get('/data-min-claim2', 'InsentiveController@data_min_claim2')->name('data-min-claim2');
Route::get('/hitung', 'InsentiveController@hitung')->name('hitung');
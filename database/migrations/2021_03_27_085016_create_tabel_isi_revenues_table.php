<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTabelIsiRevenuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tabel_isi_revenue', function (Blueprint $table) {
            $table->increments('revenue_id');
            $table->string('department_id');
            $table->string('employee_id');
            $table->string('bulan');
            $table->string('tahun');
            $table->string('revenue');
            $table->string('hitung_revenue_insentive');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tabel_isi_revenue');
    }
}

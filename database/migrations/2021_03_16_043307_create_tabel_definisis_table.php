<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTabelDefinisisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tabel_definisi_new_customer', function (Blueprint $table) {
            $table->increments('id_new_customer');
            $table->string('percentage');
            $table->string('min_revenue');
            $table->string('max_revenue');
            $table->date('tanggal_terbit');
            $table->date('tanggal_kedaluarsa');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tabel_definisi');
    }
}

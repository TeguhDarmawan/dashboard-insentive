<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTabelRevenueInsentivesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tabel_revenue_insentive', function (Blueprint $table) {
            $table->increments('employee_id');
            $table->string('januari_2021');
            $table->string('februari_2021');
            $table->string('maret_2021');
            $table->string('april_2021');
            $table->string('mei_2021');
            $table->string('juni_2021');
            $table->string('juli_2021');
            $table->string('agustus_2021');
            $table->string('september_2021');
            $table->string('oktober_2021');
            $table->string('november_2021');
            $table->string('desember_2021');
            $table->string('januari_2022');
            $table->string('februari_2022');
            $table->string('maret_2022');
            $table->string('april_2022');
            $table->string('mei_2022');
            $table->string('juni_2022');
            $table->string('juli_2022');
            $table->string('agustus_2022');
            $table->string('september_2022');
            $table->string('oktober_2022');
            $table->string('november_2022');
            $table->string('desember_2022');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tabel_revenue_insentive');
    }
}

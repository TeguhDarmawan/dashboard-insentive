<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTabelDefinisiRoyaltyCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tabel_definisi_royalty_customer', function (Blueprint $table) {
            $table->increments('id_royalty_customer');
            $table->string('percentage');
            $table->date('tanggal_terbit');
            $table->date('tanggal_kedaluarsa');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tabel_definisi_royalty_customer');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTabelIsiRevenueRoyaltiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tabel_isi_revenue_royalty', function (Blueprint $table) {
            $table->increments('revenue_royalty_id');
            $table->string('department_id');
            $table->string('employee_id');
            $table->string('revenue');
            $table->string('hitung_revenue_royalty');
            $table->date('date_revenue');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tabel_isi_revenue_royalty');
    }
}


<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Dashboard Insentive</title>
  <!-- Logo Website -->
  <link rel="shortcut icon" href="{{ asset('img/matrix.png') }}">
  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="{{ asset('AdminLTE/plugins/fontawesome-free/css/all.min.css') }}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('AdminLTE/dist/css/adminlte.min.css') }}">
</head>
<body class="hold-transition layout-top-nav">
<div class="wrapper">
<nav class="main-header navbar navbar-expand navbar-indigo navbar-dark">
    <div class="container">
      <a href="#" class="navbar-brand">
        <!-- <img src="{{ asset('img/matrixlogo.png') }}" style="width:100px;"  alt="User Image"> -->
        <span class="brand-text font-weight-light">PT NAP Info Lintas Nusa</span>
      </a>
    </div>
    
    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <li class="nav-item">
        <a class="nav-link" data-widget="fullscreen" href="#" role="button">
          <i class="fas fa-expand-arrows-alt"></i>
        </a>
      </li>
    </ul>
</nav>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper bg-white">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0"> Tabel Insentive New Revenue</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
            <button class="btn bg-gradient-success"><li class="breadcrumb-item"><a href="{{ route('tabel-isi-data-revenue')}}" style="color:white">Kembali ke Dashboard Utama</a></li></button>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <div class="card table-responsive">
              <table border="1">
                <tr>
                <td width="10 px">No.</td>
                <td width="150 px">Employee Name</td>
                <td width="100 px">Employee ID</td>
                <td width="100 px">Min Claim</td>
                <td>Jan</td>
                <td>Feb</td>
                <td>Mar</td>
                <td>Apr</td>
                <td>May</td>
                <td>Jun</td>
                <td>Jul</td>
                <td>Aug</td>
                <td>Sep</td>
                <td>Okt</td>
                <td>Nov</td>
                <td>Des</td>
                </tr>
                @php $no = 1 @endphp
                @foreach($test ?? '' as $test)
                <tr>
                <td>{{ $no++ }}</td>
                <!-- <td>{{ $test->int_emp_id }}</td> -->
                <td>{{ $test->int_emp_name }}</td>
                <td>{{ $test->int_emp_number }}</td>
                <td>{{ number_format($test->min_claim) }}</td>
                <td>{{ number_format($test->Jan) }}</td>
                <td>{{ number_format($test->Feb) }}</td>
                <td>{{ number_format($test->Mar) }}</td>
                <td>{{ number_format($test->Apr) }}</td>
                <td>{{ number_format($test->May) }}</td>
                <td>{{ number_format($test->Jun) }}</td>
                <td>{{ number_format($test->Jul) }}</td>
                <td>{{ number_format($test->Aug) }}</td>
                <td>{{ number_format($test->Sep) }}</td>
                <td>{{ number_format($test->Oct) }}</td>
                <td>{{ number_format($test->Nov) }}</td>
                <td>{{ number_format($test->Des) }}</td>
                </tr>
                @endforeach
                <!-- <tfoot>
                <tr>
                <td colspan="4" style="text-align:center">Total</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                </tr>
                </tfoot> -->
                </table>
          </div>
          <!-- /.col-md-6 -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
</div>

  <!-- Main Footer -->
  <footer class="main-footer">
  <strong>Copyright &copy; <?php  echo date('Y'); ?>  <a href="https://napinfo.co.id" target='_blank'>PT NAP Info Lintas Nusa</a>.</strong> All rights reserved.
  </footer>
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->

<!-- jQuery -->
<script src="{{ asset('AdminLTE/plugins/jquery/jquery.min.js') }}"></script>
<!-- Bootstrap 4 -->
<script src="{{ asset('AdminLTE/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('AdminLTE/dist/js/adminlte.min.js') }}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{ asset('AdminLTE/dist/js/demo.js') }}"></script>
</body>
</html>

    

    

<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<head>
    @include('Template.head')
    <meta name="viewport" content="width=device-width, initial-scale=1">
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script> -->
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" /> -->
    <!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>             -->
    <script src="https://markcell.github.io/jquery-tabledit/assets/js/tabledit.min.js" defer></script>
</head>
<body class="hold-transition sidebar-mini layout-navbar-fixed">
<div class="wrapper">

  <!-- Navbar -->
    @include('Template.navbar')
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
    @include('Template.sidebar')  
  <!-- / .Main Sidebar Container -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper bg-white">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="content-header">
          <h1> Tabel Isi Data Revenue</h1>
          <br>
          <button class="btn bg-gradient-success"><i class="fas fa-plus-square"></i><a href="{{ route('tambah-isi-data-revenue') }}" style="color:white"> Tambah Isi Data Revenue</button></a>
          <!-- <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal-isidata"><i class="fas fa-plus-square"></i>
           Tambah isi data revenue</button> -->
          </div><!-- /.col -->
          <div class="col-sm-6">
            <!-- <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Data Karyawan</li>
            </ol> -->
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <div class="modal fade" id="modal-isidata">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Tambah isi data revenue</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <form action="/tambah/proses/data-revenue" enctype="multipart/form-data" method="post">
            {{ csrf_field() }}
            <div class="modal-body">
              <div class="form-group">
                <label>Department</label>
                <br>
                <select class="form-control productcategory" name="department_id" id="int_emp_department" required>
                    <option value>Department</option>
                    @foreach($data_department ?? '' as $data_department)
                    <option value="{{ $data_department->department_id }}">{{ $data_department->department_name }}</option>
                    @endforeach
                </select>  
              </div>
     
              <div class="form-group">
              <b>Employee Name</b>
                <select class="form-control int_emp_name" name="employee_id" required>
                    <option value="" disabled="true" selected="true">Cari Employee Name</option>
                </select>
              </div>

              <div class="form-row">
              <div class="col-md-6">
                <div class="form-group">
                  <b>Bulan</b>
                    <select class="form-control" name="bulan" required>
                        <option value>Pilih Bulan</option>
                        <option value="Jan">Januari</option>
                        <option value="Feb">Februari</option>
                        <option value="Mar">Maret</option>
                        <option value="Apr">April</option>
                        <option value="May">Mei</option>
                        <option value="Jun">Juni</option>
                        <option value="Jul">July</option>
                        <option value="Aug">Agustus</option>
                        <option value="Sep">September</option>
                        <option value="Oct">Oktober</option>
                        <option value="Nov">November</option>
                        <option value="Dec">Desember</option>
                    </select>  
                </div>
              </div>
                <div class="col-md-6">
                    <div class="form-group">
                    <b>Tahun</b>
                    <select class="form-control" name="bulan" required>
                        <option value>Pilih Tahun</option>
                        <option value="2020">2020</option>
                        <option value="2021">2021</option>
                        <option value="2022">2022</option>
                        <option value="2023">2023</option>
                        <option value="2024">2024</option>
                        <option value="2025">2025</option>
                        <option value="2026">2026</option>
                        <option value="2027">2027</option>
                        <option value="2028">2028</option>
                        <option value="2029">2029</option>
                        <option value="2030">2030</option>
                    </select>  
                    </div>
                </div>
            </div>

              <div class="form-group">
                <label>Revenue</label>
                    <input name="" class="form-control currency" autocomplete="off" placeholder="Masukkan digit angka, contoh : 5,000,000" value="" required/>
                    <input name="revenue" type="hidden">
              </div>
            
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <input type="submit" name="submit" class="btn btn-primary float-right" value="Save Data">
              </form>
            </div>
          </div>
 
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->

   <!-- Main content -->
   <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header bg-indigo">
                <h3 class="card-title">Tabel Isi Revenue</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body table-responsive"> 
              {{ csrf_field() }}
              <table id="editable" class="table table-bordered table-striped example4">
              <thead>
                <tr>
                    <td colspan="27" style="text-align:center"><b>Booked Revenue 2021<b></td>
                </tr>
                <tr>
                    <th>No</th>
                    <th>Department</th>
                    <th>Employee Name</th>
                    <th>Revenue Date</th>
                    <!-- <th>Tahun</th> -->
					          <th>Revenue</th>
                    <th>Action</th>
					          <!-- <th>Jun</th>
                    <th>Jul</th>
				            <th>Aug</th>
                    <th>Sep</th>
					          <th>Oct</th>
                    <th>Nov</th>
					          <th>Dec</th> -->
                </tr>
                </thead>
                <tbody>
                @php $no = 1 @endphp
                @foreach($tabel_revenue ?? '' as $tr)
          		  <tr>
                  <td>{{ $no++ }}</td>
					        <td>{{ $tr->nama_departemen }}</td>
                  <td>{{ $tr->nama_karyawan }}</td>
					        <td>{{Carbon\Carbon::parse($tr->date_revenue)->format("d/m/Y")}}</td>
                  <!-- <td>{{ $tr->tahun }}</td> -->
					        <td>{{ number_format($tr->revenue) }}</td>
                  <td>
                  
                    <button class="btn bg-gradient-primary"><i class="fas fa-pencil-alt"></i><a href="{{url('editdatarevenue/'.$tr->revenue_id)}}" style="color:white;"> Edit Revenue</button>
                    <!-- <br>
                    <button class="btn bg-gradient-danger"><a href="{{url('hapusdatarevenue/'.$tr->revenue_id)}}" style="color:white;" >Hapus Revenue</button> -->                  
                  </td>
                  <!-- <td>{{ $tr->mei_2021 }}</td>
					        <td>{{ $tr->juni_2021 }}</td>
                  <td>{{ $tr->juli_2021 }}</td>
				        	<td>{{ $tr->agustus_2021 }}</td>
                  <td>{{ $tr->september_2021 }}</td>
					        <td>{{ $tr->oktober_2021 }}</td>
                  <td>{{ $tr->november_2021 }}</td>
					        <td>{{ $tr->desember_2021 }}</td> -->
				        </tr>
                @endforeach
			  </tbody>
              </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>  
    <!-- /.content -->
    <a id="back-to-top" href="#" class="btn btn-primary back-to-top" role="button" aria-label="Scroll to top">
      <i class="fas fa-chevron-up"></i>
    </a>
  </div>
  <!-- /.content-wrapper -->

  <!-- Main Footer -->
  <footer class="main-footer">
        @include('Template.footer')
        @include('sweetalert::alert')
   </footer>
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->
    @include('Template.script')
    <script src="{{ asset('AdminLTE/jquery.maskMoney.min.js') }}"></script>
<!-- /.REQUIRED SCRIPTS -->
</body>
</html>

<script>
		function updateNewPrice() {
			var oldPrice = document.getElementsByName("old_price")[0].value;
			var discountPrct = document.getElementsByName("percent")[0].value;	
			if (!isNaN(oldPrice) && !isNaN(discountPrct)) {
				var discount = (oldPrice / 100) * discountPrct;
				if (!isNaN(discount) > 0)
					document.getElementsByName("new_price")[0].value = discount;
			}
		}
	</script>

<!-- <script>
		function updateNewPrice() {
			var oldPrice = document.getElementsByName("old_price")[0].value;
			var discountPrct = document.getElementsByName("percent")[0].value;	
			if (isNaN(oldPrice) == isNaN(discountPrct)) {
				//var discount = (oldPrice / 100) * discountPrct;
				var count = (discountPrct / 100) * oldPrice;
				var discount = oldPrice - count;
				if (!isNaN(discount) > 0)
                {
					document.getElementsByName("new_price")[0].value = discount;
			}
		}
	</script> -->


<!-- <script type="text/javascript">
$(document).ready(function(){
   
  $.ajaxSetup({
    headers:{
      'X-CSRF-Token' : $("input[name=_token]").val()
    }
  });

  $('#editable').Tabledit({
    url:'{{ route("tabel-isi-data-revenue.action") }}',
    dataType:"json",
    columns:{
      identifier:[0, 'employee_id'],
      editable:[[1, 'employee_name'], [2, 'januari_2021'], [3, 'februari_2021'], [4, 'maret_2021'],
      [5, 'april_2021']
      , [6, 'mei_2021'], [7, 'juni_2021'],
      [8, 'juli_2021'], [9, 'agustus_2021'], [10, 'september_2021'],
      [11, 'oktober_2021'], [12, 'november_2021'], [13, 'desember_2021']]
    },                                   
    restoreButton:false,
    onSuccess:function(data, textStatus, jqXHR)
    {
      if(data.action == 'delete')
      {
        $('#'+data.id).remove();
      }
    }
  });
});  
</script> -->

<script>
    $(document).ready(function() 
    {
    $('.selectsearch').select2();
    });
</script>

<script>
  $(function() {
    $('.currency').maskMoney({precision:0});
  })

  $('.currency').change(function() {
        $(this).next('input').val( $(this).val().replaceAll(',','') );
  });
</script>

<script type="text/javascript">
	$(document).ready(function(){

		$(document).on('change','.productcategory',function(){
			// console.log("hmm its change");

			var cat_id=$(this).val();
			// console.log(cat_id);
			var div=$(this).parent().parent().parent();

			var op=" ";

			$.ajax({
				type:'get',
				url:'{!!URL::to('findSalesEmployeeName')!!}',
				data:{'id':cat_id},
				success:function(data){
					op+='<option value="0" selected disabled>Cari Employee Name</option>';
					for(var i=0;i<data.length;i++){
					op+='<option value="'+data[i].int_emp_id+'">'+data[i].int_emp_name+'</option>';
				   }

				   div.find('.int_emp_name').html(" ");
				   div.find('.int_emp_name').append(op);
				},
				error:function(){

				}
			});
		});
  });
</script>

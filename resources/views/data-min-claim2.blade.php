
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<head>
    @include('Template.head')
</head>
<body class="hold-transition sidebar-mini layout-navbar-fixed">
<div class="wrapper">

  <!-- Navbar -->
    @include('Template.navbar')
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
    @include('Template.sidebar')  
  <!-- / .Main Sidebar Container -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper bg-white">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="content-header">
          <h1>Data Min Claim</h1>
          <br>
          <button class="btn bg-gradient-success"><i class="fas fa-plus-square"></i><a href="{{ route('tambah-min-claim') }}" style="color:white"> Tambah Min Claim</button></a>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <!-- <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Data Karyawan</li>
            </ol> -->
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

   <!-- Main content -->
   <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header bg-indigo">
                <h3 class="card-title">Data Min Claim</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body"> 
              <table id="" class="table table-bordered table-striped example4">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Employee Name</th>
                  <!-- <th>Employee ID</th> -->
                  <th>Department</th>
                  <th>Min Claim</th>
				</tr>
			  </thead>
              <tbody>
                @php $no = 1 @endphp
                @foreach($nama_karyawan_sales ?? '' as $ts)
          		  <tr>
                  <td>{{ $no++ }}</td>
                  <td>{{ $ts->sales_team_name }}</td>
                  @endforeach
                  <td></td>
				  <td></td>
				  </tr>  
			  </tbody>
              </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>  
    <!-- /.content -->
    <a id="back-to-top" href="#" class="btn btn-primary back-to-top" role="button" aria-label="Scroll to top">
      <i class="fas fa-chevron-up"></i>
    </a>
  </div>
  <!-- /.content-wrapper -->

  <!-- Main Footer -->
  <footer class="main-footer">
        @include('Template.footer')
        @include('sweetalert::alert')
   </footer>
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->
    @include('Template.script')

<!-- /.REQUIRED SCRIPTS -->
</body>
</html>

<script>
		function updateNewPrice() {
			var oldPrice = document.getElementsByName("old_price")[0].value;
			var discountPrct = document.getElementsByName("percent")[0].value;	
			if (!isNaN(oldPrice) && !isNaN(discountPrct)) {
				var discount = (oldPrice / 100) * discountPrct;
				if (!isNaN(discount) > 0)
					document.getElementsByName("new_price")[0].value = discount;
			}
		}
	</script>

<script>
    $(document).ready(function() 
    {
    $('.selectsearch').select2();
    });
</script>

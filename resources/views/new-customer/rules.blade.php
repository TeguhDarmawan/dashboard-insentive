
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<head>
    @include('Template.head')
</head>
<body class="hold-transition sidebar-mini layout-navbar-fixed">
<div class="wrapper">

  <!-- Navbar -->
    @include('Template.navbar')
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
    @include('Template.sidebar')  
  <!-- / .Main Sidebar Container -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper bg-white">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="content-header">
          <h4> Tabel Definisi #New Customer</h4>
          <br>
          <button class="btn bg-gradient-success"><i class="fas fa-plus-square"></i><a href="{{ route('tambah-rules') }}" style="color:white"> Tambah Rules</button></a>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <!-- <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Data Karyawan</li>
            </ol> -->
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

   <!-- Main content -->
   <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header bg-indigo">
                <h3 class="card-title">New Customer</h3>
              </div>
              <!-- /.card-header -->
              <div id="contain">
                <div id="right">
                <br>
                </div>
              </div>
              <div class="card-body">
              <table id="" class="table table-bordered table-striped example4">
              <thead>
						<tr>
              <th>No</th>
							<th>Min Revenue</th>
              <th>Percentage</th>
							<th>Max Revenue</th>
							<th>Tanggal Terbit</th>
							<th>Tanggal Kedaluarsa</th>
              <th>Action</th>
						</tr>
					</thead>
          <tbody>
            @php $no = 1 @endphp
            @foreach($new_customer ?? '' as $nc)
						<tr>
              <td>{{ $no++ }}</td>
              <td>{{number_format($nc->min_revenue)}}</td>
							<td>{{$nc->percentage}} &#37;</td>
							<td>{{number_format($nc->max_revenue)}}</td>
							<td>{{Carbon\Carbon::parse($nc->tanggal_terbit)->format("d/m/Y")}}</td>
							<td>{{$nc->tanggal_kedaluarsa ? Carbon\Carbon::parse($nc->tanggal_kedaluarsa)->format("d/m/Y") : 'Status Masih Aktif'}}</td>
              <td>
              <button class="btn bg-gradient-primary"><i class="fas fa-pencil-alt"></i><a href="{{url('editdatarules/'.$nc->id_new_customer)}}" style="color:white;" > Edit Rules</button>
              </td>
						</tr>
            @endforeach
					</tbody>
          </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>  
    <!-- /.content -->
    <a id="back-to-top" href="#" class="btn btn-primary back-to-top" role="button" aria-label="Scroll to top">
      <i class="fas fa-chevron-up"></i>
    </a>
  </div>
  <!-- /.content-wrapper -->

  <!-- Main Footer -->
  <footer class="main-footer">
        @include('Template.footer')
        @include('sweetalert::alert')
   </footer>
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->
    @include('Template.script')

<!-- /.REQUIRED SCRIPTS -->


</body>
</html>






<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<head>
    @include('Template.head')   
</head>
<body class="hold-transition sidebar-mini layout-navbar-fixed">
<div class="wrapper">

  <!-- Navbar -->
    @include('Template.navbar')
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
    @include('Template.sidebar')  
  <!-- / .Main Sidebar Container -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper bg-white">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
          <h1 class="m-0">Tambah Min Claim</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <button class="btn bg-gradient-success"><li class="breadcrumb-item"><a href="{{ route('data-min-claim')}}" style="color:white">Kembali Data Min Claim</a></li></button>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <!-- /.card-header -->
              <div class="card-body">
              @if(count($errors) > 0)
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                    {{ $error }} <br/>
                    @endforeach
                </div>
                @endif

              <!-- START ALERTS AND CALLOUTS -->
                <div class="row">
                <div class="col-md-12">
                <div class="card card-default">
                
                <!-- /.card-header -->
        <form class="form-detail" action="/tambah/proses/data-min-claim" enctype="multipart/form-data" method="POST" id="myform">
        {{ csrf_field() }}
                
        <div class="card-body">

        <div class="form-row">
          <div class="col-md-6">
            <div class="form-group">
                    <b>Department</b>
                    <select class="form-control productcategory" name="department_id" id="int_emp_department">
                    <option value>Department</option>
                    @foreach($data_department ?? '' as $data_department)
                    <option value="{{ $data_department->department_id }}">{{ $data_department->department_name }}</option>
                    @endforeach
                </select>  
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                <b>Employee Name</b>
                <select class="form-control int_emp_name" name="employee_id">
                    <option value="" disabled="true" selected="true">Cari Employee Name</option>
                </select>   
                </div>
            </div>
        </div>

        <div class="form-row">
            <div class="col-md-6">
                <div class="form-group">
                <label>Min Claim</label>
                    <input name="" class="form-control currency" autocomplete="off" placeholder="Masukkan digit angka, contoh : 5,000,000" value=""/>
                    <input name="min_claim" type="hidden">
                </div>
            </div>
        </div>

        <div class="form-detail align-items-center justify-content">
        <input type="submit" name="submit" class="btn btn-primary float-right" value="Tambah Data">
        </form>
        </div>  
      </div>
      <!-- /.card-body -->
    </div>
    <!-- /.card -->
  </div>
  <!-- /.col -->
</div>
<!-- /.row -->
<!-- END ALERTS AND CALLOUTS -->
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>  
    <!-- /.content -->
    <a id="back-to-top" href="#" class="btn btn-primary back-to-top" role="button" aria-label="Scroll to top">
      <i class="fas fa-chevron-up"></i>
    </a>
  </div>
  <!-- /.content-wrapper -->

  <!-- Main Footer -->
  <footer class="main-footer">
        @include('Template.footer')
        @include('sweetalert::alert')
   </footer>
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->
    @include('Template.script')
    
<!-- /.REQUIRED SCRIPTS -->
<script src="{{ asset('AdminLTE/jquery.maskMoney.min.js') }}"></script>

</body>
</html>

<script>
  $(function() {
    $('.currency').maskMoney({precision:0});
  })

  $('.currency').change(function() {
        $(this).next('input').val( $(this).val().replaceAll(',','') );
  });
</script>

<script>
    $(document).ready(function() 
    {
    $('.selectsearch').select2();
    });
</script>

<script type="text/javascript">
	$(document).ready(function(){

		$(document).on('change','.productcategory',function(){
			// console.log("hmm its change");

			var cat_id=$(this).val();
			// console.log(cat_id);
			var div=$(this).parent().parent().parent();

			var op=" ";

			$.ajax({
				type:'get',
				url:'{!!URL::to('findSalesEmployeeName')!!}',
				data:{'id':cat_id},
				success:function(data){
					op+='<option value="0" selected disabled>Cari Employee Name</option>';
					for(var i=0;i<data.length;i++){
					op+='<option value="'+data[i].int_emp_id+'">'+data[i].int_emp_name+'</option>';
				   }

				   div.find('.int_emp_name').html(" ");
				   div.find('.int_emp_name').append(op);
				},
				error:function(){

				}
			});
		});
  });
</script>


<script>
	$(document).ready(function(){
		$(document).on('change','.department_name',function () {
			var dept_id=$(this).val();
			var a=$(this).parent().parent().parent().parent();
			console.log(dept_id);
			var op="";
			$.ajax({
				type:'get',
				url:'{!!URL::to('findDepartmentName')!!}',
				data:{'id':dept_id},
				dataType:'json',//return data will be json
				success:function(data){
					console.log("department_name");
					console.log(data.department_name);

					// here price is coloumn name in products table data.coln name
					
                    a.find('.pic').val(data.department_name);
				
                },
				error:function(){

				}
			});
		});
	});
</script>



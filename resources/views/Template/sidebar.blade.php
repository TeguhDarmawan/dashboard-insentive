<aside class="main-sidebar sidebar-light-indigo elevation-4">
        <!-- Brand Logo -->
        <a href="#" class="brand-link bg-indigo">
            <span class="brand-text font-weight-light">PT NAP Info Lintas Nusa</span>
        </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image" style="margin: 0 auto;">
          <img src="{{ asset('img/matrixlogo.png') }}" style="width:100px;" class="img-circle elevation-20" alt="User Image">
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item menu-open">
            <a href="#" class="nav-link active">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard Insentive
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>

            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ route('beranda-insentive') }}" class="nav-link">
                <i class="nav-icon fas fa-home"></i>
                  <p>Beranda</p>
                </a>
              </li>

              <li class="nav-item">
              <a href="#" class="nav-link">
                <i class="nav-icon fas fa-database"></i>
                <p>
                  Tabel #1 (Definisi)
                  <i class="fas fa-angle-left right"></i>
                </p>
              </a>

              <ul class="nav nav-treeview">
                <li class="nav-item">
                  <a href="#" class="nav-link">
                    <i class="far fa-list-alt nav-icon"></i>
                    <p>
                      Category
                      <i class="fas fa-angle-left right"></i>
                    </p>
                  </a>

                <ul class="nav nav-treeview">
                  <li class="nav-item">
                    <a href="#" class="nav-link">
                      <i class="nav-icon"></i>
                      <p>1. New Customer</p>
                      <i class="fas fa-angle-left right"></i>
                    </a>

                  <ul class="nav nav-treeview">
                    <li class="nav-item">
                    <a href="{{ route('rules') }}" class="nav-link">
                      <i class="nav-icon"></i>
                      <p>Rules</p>
                    </a>
                  </li>
                  </ul>
                  
                  <li class="nav-item">
                    <a href="{{ route('data-royalty-customer') }}" class="nav-link">
                      <i class="nav-icon"></i>
                      <p>2. Royalty Customer</p>
                    </a>
                  </li>

                  <li class="nav-item">
                    <a href="#" class="nav-link">
                      <i class="nav-icon"></i>
                      <p>3. Churn Customer</p>
                    </a>
                  </li>

                  <li class="nav-item">
                    <a href="#" class="nav-link">
                      <i class="nav-icon"></i>
                      <p>4. Change Service</p>
                    </a>
                  </li>

                  <li class="nav-item">
                    <a href="#" class="nav-link">
                      <i class="nav-icon"></i>
                      <p>5. Overriding</p>
                    </a>
                  </li>
                </ul>
              </li>
            </ul>

            <ul class="nav nav-treeview">
                <li class="nav-item">
                  <a href="{{ route('data-min-claim') }}" class="nav-link">
                    <i class="far fa-calendar-check nav-icon"></i>
                    <p>  Add Min Claim
                    </p>
                  </a>
            </li>
            </ul>
            </li>
            </ul>

             <!-- <li class="nav-item">
              <a href="{{ route('data-min-claim2') }}" class="nav-link">
              <i class="far fa-calendar-check nav-icon"></i>
                <p>Add Min Claim v.2</p>
              </a>
            </li> -->

            <!--
            <li class="nav-item">
              <a href="#" class="nav-link">
              <i class="fas fa-database nav-icon"></i>
                <p>Tabel Sales</p>
              </a>
            </li> -->

            <!-- <li class="nav-item">
              <a href="{{ route('hitung') }}" class="nav-link">
              <i class="fas fa-database nav-icon"></i>
                <p>Hitung</p>
              </a>
            </li>  -->

            <li class="nav-item">
              <a href="{{ route('tabel-isi-data-revenue') }}" class="nav-link">
              <i class="fas fa-database nav-icon"></i>
                <p>Tabel #2 (Isi data revenue)</p>
              </a>
            </li>

            <li class="nav-item">
              <a href="{{ route('data-new-revenue') }}" class="nav-link">
              <i class="fas fa-database nav-icon"></i>
                <p>Tabel #3 (New revenue)</p>
              </a>
            </li>

            <li class="nav-item">
              <a href="#" class="nav-link">
                <i class="fas fa-database nav-icon"></i>
                <p>Tabel #4 (Royalty)</p>
                <i class="right fas fa-angle-left"></i>
              </a>

              <ul class="nav nav-treeview">
                <li class="nav-item">
                <a href="{{ route('isi-data-revenue-royalty') }}" class="nav-link">
                  <i class="nav-icon"></i>
                  <p>Isi data revenue royalty</p>
                </a>
              </li>

              <li class="nav-item">
                <a href="{{ route('data-revenue-royalty') }}" class="nav-link">
                  <i class="nav-icon"></i>
                  <p>Tabel royalty</p>
                </a>
              </li>
              </ul>
            </li>
    

          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
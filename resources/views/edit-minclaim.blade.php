<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<head>
    @include('Template.head')   
</head>
<body class="hold-transition sidebar-mini layout-navbar-fixed">
<div class="wrapper">

  <!-- Navbar -->
    @include('Template.navbar')
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
    @include('Template.sidebar')  
  <!-- / .Main Sidebar Container -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper bg-white">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
          <h1 class="m-0">Edit Isi Data Revenue</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <button class="btn bg-gradient-success"><li class="breadcrumb-item"><a href="{{ route('rules')}}" style="color:white">Kembali Data Rules</a></li></button>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <!-- /.card-header -->
              <div class="card-body">
              @if(count($errors) > 0)
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                    {{ $error }} <br/>
                    @endforeach
                </div>
                @endif

              <!-- START ALERTS AND CALLOUTS -->
                <div class="row">
                <div class="col-md-12">
                <div class="card card-default">
                
                <!-- /.card-header -->
        <form class="form-detail" action="{{ url('update-proses-minclaim',$data_min_claim->sales_id) }}" enctype="multipart/form-data" method="POST" id="myform">
        {{ csrf_field() }}
        @method('patch')
                
                <div class="card-body">
                <div class="form-row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <b>Department</b>
                            @foreach($data_min_claim_join ?? '' as $data_min_claim_join)
                            @endforeach
                            <input class="form-control" name="department_id" type="text" value="{{ $data_min_claim_join->nama_departemen }}" autocomplete="off" readonly>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <b>Employee Name</b>
                            <input class="form-control" name="employee_id" type="text" value="{{ $data_min_claim_join->nama_karyawan }}" autocomplete="off" readonly>
                        </div>
                    </div>
                </div>

                <div class="form-row">
                  <div class="col-md-6">
                      <div class="form-group">
                      <label>Min Claim</label>
                          <input name="" class="form-control currency" value="{{ number_format($data_min_claim->min_claim) }}" type="text" autocomplete="off"/>
                          <input name="min_claim" type="hidden" value="{{ $data_min_claim->min_claim }}">
                      </div>
                  </div>
              </div>
                <div class="form-detail align-items-center justify-content">
                <input type="submit" name="submit" class="btn btn-primary float-right" value="Submit Data">
                </form>
        </div>  
      </div>
      <!-- /.card-body -->
    </div>
    <!-- /.card -->
  </div>
  <!-- /.col -->
</div>
<!-- /.row -->
<!-- END ALERTS AND CALLOUTS -->
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>  
    <!-- /.content -->
    <a id="back-to-top" href="#" class="btn btn-primary back-to-top" role="button" aria-label="Scroll to top">
      <i class="fas fa-chevron-up"></i>
    </a>
  </div>
  <!-- /.content-wrapper -->

  <!-- Main Footer -->
  <footer class="main-footer">
        @include('Template.footer')
   </footer>
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->
    @include('Template.script')
    @include('sweetalert::alert')
<!-- /.REQUIRED SCRIPTS -->

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.6/jquery.min.js" type="text/javascript"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js" type="text/javascript"></script>
<link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/start/jquery-ui.css"
        rel="Stylesheet" type="text/css"/>
<script src="{{ asset('AdminLTE/jquery.maskMoney.min.js') }}"></script>
</body>
</html>


<script>          
function CheckUncheckFunction() 
{ 
  if (document.getElementById('cek').checked) 
  { 
    
    document.getElementById('tanggalkedaluarsa').value="";
    $('#tanggalkedaluarsa').attr("disabled", true);
  } 
      
  else
  { 
    document.getElementById('tanggalkedaluarsa').value=""; 
    $('#tanggalkedaluarsa').val("");
    $('#tanggalkedaluarsa').removeAttr('disabled', false);
    $("#tanggalkedaluarsa").prop("readOnly", false); 
  } 
}
</script>

<script type="text/javascript">
        $(function () {
            $("#txtFrom").datepicker({
                numberOfMonths: 2,
                onSelect: function (selected) {
                    var dt = new Date(selected);
                    dt.setDate(dt.getDate() + 1);
                    $(".txtTo").datepicker("option", "minDate", dt);
                }
            });
            $(".txtTo").datepicker({
                numberOfMonths: 2,
                onSelect: function (selected) {
                    var dt = new Date(selected);
                    dt.setDate(dt.getDate() - 1);
                    $("#txtFrom").datepicker("option", "maxDate", dt);
                }
            });
        });
    </script>

<script>
  $(function() {
    $('.currency').maskMoney({precision:0});
  })

  $('.currency').change(function() {
        $(this).next('input').val( $(this).val().replaceAll(',','') );
  });
</script>

<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<head>
    @include('Template.head')
</head>
<body class="hold-transition sidebar-mini layout-navbar-fixed">
<div class="wrapper">

  <!-- Navbar -->
    @include('Template.navbar')
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
    @include('Template.sidebar')  
  <!-- / .Main Sidebar Container -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper bg-white">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="content-header">
          <h1> Hitung Percentage</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <!-- <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Data Karyawan</li>
            </ol> -->
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

   <!-- Main content -->
   <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header bg-indigo">
                <h3 class="card-title">Hitung Revenue Percentage</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body"> 
              <form method="post" action="" enctype="multipart/form-data">
                Input Revenue : <input class="form-control" onkeydown="updateNewPrice()" type="name" name="old_price" value=""><br>
                <input class="form-control" onkeydown="updateNewPrice()" type="number" name="percent" value="">
                Hasilnya : <input class="form-control" type="name" name="new_price" value=""><br>
                <!-- <input type="submit" id="submit" value="submit"> -->
                </form>

                <!-- <button onclick="myFunction()">Try it</button> -->
                <script language='javascript'>
                var amt=0, dis= 0;
                amt = prompt("Hitung Revenue Insentive", " Input Revenue");
                if(amt < 100){
                document.write("Maaf! revenue yang anda masukkan tidak masuk ke dalam perhitungan Insentive.");
                }
                if(amt < 25000001){
                document.write("Maaf! revenue yang anda masukkan tidak masuk ke dalam perhitungan Insentive.");
                }
                if((amt >=25000001) && (amt <= 50000000)){
                    discount = 2.0;
                }
                if((amt >=50000001) && (amt <= 75000000)){
                    discount = 3.0;
                }
                if((amt >=75000000) && (amt <= 100000000)){
                    discount = 4.0;
                }
                if(amt >=100000001){
                    discount = 5.0;
                }
                amt = (amt/100)*discount;
                if(discount > 0)
                {
                    document.write("Masuk Percentage " + discount + "%");
                    document.write("<br/> Revenue Insentive sebesar: " + "Rp " + amt);
                }
                if(amt >=10000000)
                {
                    document.write("<br/> Maximal Revenue Insentive sebesar Rp 10000000 atau 10 Juta")
                }
                </script>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>  
    <!-- /.content -->
    <a id="back-to-top" href="#" class="btn btn-primary back-to-top" role="button" aria-label="Scroll to top">
      <i class="fas fa-chevron-up"></i>
    </a>
  </div>
  <!-- /.content-wrapper -->

  <!-- Main Footer -->
  <footer class="main-footer">
        @include('Template.footer')
        @include('sweetalert::alert')
   </footer>
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->
    @include('Template.script')

<!-- /.REQUIRED SCRIPTS -->
</body>
</html>

<script>
		function updateNewPrice() {
			var oldPrice = document.getElementsByName("old_price")[0].value;
			var discountPrct = document.getElementsByName("percent")[0].value;	
			if (!isNaN(oldPrice) && !isNaN(discountPrct)) {
				var discount = (oldPrice / 100) * discountPrct;
				if (!isNaN(discount) > 0)
					document.getElementsByName("new_price")[0].value = discount;
			}
		}
	</script>

<!-- <script>
		function updateNewPrice() {
			var oldPrice = document.getElementsByName("old_price")[0].value;
			var discountPrct = document.getElementsByName("percent")[0].value;	
			if (isNaN(oldPrice) == isNaN(discountPrct)) {
				//var discount = (oldPrice / 100) * discountPrct;
				var count = (discountPrct / 100) * oldPrice;
				var discount = oldPrice - count;
				if (!isNaN(discount) > 0)
                {
					document.getElementsByName("new_price")[0].value = discount;
			}
		}
	</script> -->


  <!-- <script>
		function updateNewPrice() {
			var oldPrice = document.getElementsByName("old_price")[0].value;
			var discountPrct = document.getElementsByName("percent")[0].value;	
			if (isNaN(oldPrice) == isNaN(discountPrct)) {
				//var discount = (oldPrice / 100) * discountPrct;
				var count = (discountPrct / 100) * oldPrice;
				var discount = oldPrice - count;
				if (!isNaN(discount) > 0)
                {
					document.getElementsByName("new_price")[0].value = discount;
			}
		}
	</script> -->

 <!-- Modal tambah rules -->
 <!-- <div class="modal fade" id="tambah-rules">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Tambah Rules</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
                
                @if(count($errors) > 0)
                <div class="alert alert-danger" role="alert">
                @foreach ($errors->all() as $error)
                    {{ $error }} <br/>
                @endforeach
                </div> 
                @endif
            <form class="form-detail" action="/tambah/proses/rules" enctype="multipart/form-data" method="POST" id="myform">
            {{ csrf_field() }}
            <div class="modal-body">
           
              <label>Percentage</label>
              <input class="form-control" type="text" name="percentage" required>
              <br>
              <label>Min Revenue</label>
              <input class="form-control" type="text" name="min_revenue" required>
              <br>
              <label>Max Revenue</label>
              <input class="form-control" type="text" name="max_revenue" required>
              <br>
            <label>Tanggal Terbit</label>
              <input class="form-control" type="date" name="tanggal_terbit" required>
              <br>
              <label>Tanggal Kedaluarsa</label>
              <input class="form-control" type="date" name="tanggal_kedaluarsa"> -->
<!-- 
              <div class="form-row">
                  <div class="col-md-6">
                      <div class="form-group">
                      <label>Tanggal Terbit</label>
                          <input name="tanggal_terbit" class="form-control" id="txtFrom" required/>
                      </div>
                  </div>
                  <div class="col-md-6">
                      <div class="form-group">
                      <label for="enddate">Tanggal Kedaluarsa</label>
                          <input name="tanggal_kedaluarsa" class="form-control txtTo" id="tanggalkedaluarsa" />
                      </div>
                  </div>
              </div>
              <div class="mb-1 tulisan_kiri form-group" style="align:left">
                  <label class="small mb-1" for="cek">
                  <input type="checkbox" id="cek" onchange="CheckUncheckFunction()"/>
                  Rules Masih Aktif</label>
              </div>
            </div>
            
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary">Simpan Data</button>
              </form>
            </div>
          </div> -->
          <!-- /.modal-content -->
        <!-- </div> -->
        <!-- /.modal-dialog -->
      <!-- </div>
      /.modal --> 

  <!-- <script>
		function updateNewPrice() {
			var oldPrice = document.getElementsByName("old_price")[0].value;
			var discountPrct = document.getElementsByName("percent")[0].value;	
			if (!isNaN(oldPrice) && !isNaN(discountPrct)) {
				var discount = (oldPrice / 100) * discountPrct;
				if (!isNaN(discount) > 0)
					document.getElementsByName("new_price")[0].value = discount;
			}
		}
	</script> -->